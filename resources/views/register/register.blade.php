<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
</head>
<body>
    <h1><b>Buat Account Baru!</b></h1>
    <form action="/sent" method="POST">
        <label><b>Sign Up</b></label><br><br>
        @csrf
        <label for="firstName" name="fname">First name:</label><br><br>
        <input type="text" id="firstName"><br><br>

        <label for="lastName" name="lname">Last name:</label><br><br>
        <input type="text" id="lastName">

        <br><br>
        <!-- Gender -->

        <label>Gender</label><br><br>
        <input type="radio" id="male" name="gender">
        <label for="male">Male</label>
        <br>
        <input type="radio" id="female" name="gender">
        <label for="female">Female</label>
        <br>
        <input type="radio" id="otherGender" name="gender">
        <label for="otherGender">Other</label>
        
        <br><br>
        <!-- Nationality -->

        <label>Nationality</label><br><br>
        <select name="namaNegara" id="pilihanNegara" name="negara">
            <option value="indonesia">Indonesia</option>
            <option value="singapore">Singapore</option>
            <option value="thailand">Thailand</option>
        </select>

        <br><br>
         <!-- Language Spoken-->

        <label>Language Spoken:</label><br><br>
        <input type="checkbox" id="indonesia" name="bahasa">
        <label for="indonesia">Bahasa Indonesia</label><br>
        <input type="checkbox" id="english" name="bahasa">
        <label for="english">english</label><br>
        <input type="checkbox" id="otherLanguage" name="bahasa">
        <label for="otherLanguage">Other</label>

        <br><br>
        <!-- Bio -->

        <label for="bio">Bio:</label><br><br>
        <textarea name="bioSelf" id="bio" cols="30" rows="10"></textarea><br>
        <button type="submit" value=kirim>Sign Up</button>
    </form>
</body>
</html>