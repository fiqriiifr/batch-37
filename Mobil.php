<!-- 
$sheep = new Animal("shaun");

echo $sheep->name; // "shaun"
echo $sheep->legs; // 4
echo $sheep->cold_blooded; // "no"

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded()) -->\

<?php

class Mobil{
    public $roda = 4;
    public $merk;
    public $spion = 2;
    public $bahanBakar = "Bensin";

    public function __construct($nama){
        $this->merk = $nama;
    }
}

?>