Fiqri Ramdhani (Latihan 10 mysql)

1. Buat database
CREATE DATABASE myshop;

2. Buat table

CREATE TABLE users( id int(6) AUTO_INCREMENT PRIMARY KEY, name varchar(255) NOT Null, email varchar(255) NOT Null, password varchar(255) NOT Null );

CREATE TABLE categories( id int(5) AUTO_INCREMENT PRIMARY KEY, name varchar(255) NOT NULL );

CREATE TABLE items( id int(6) AUTO_INCREMENT PRIMARY KEY, name varchar(255) NOT Null, description varchar(255) NOT Null, price int(10) NOT null, stock int(6), categories_id int(5) NOT null, FOREIGN KEY(categories_id) REFERENCES categories(id) );

3. Insert data

Users
INSERT INTO `users`(`name`, `email`, `password`) VALUES ("John Doe","john@doe.com","john123");
INSERT INTO `users`(`name`, `email`, `password`) VALUES ("Jane Doe","jane@doe.com","jenita123");

Categories
INSERT INTO `categories`(`name`) VALUES ("gadget"), ("cloth"),("men"), ("women"), ("branded");

Items
INSERT INTO `items`(`name`, `description`, `price`, `stock`, `categories_id`) VALUES ("Sumsang b50","hape keren dari merek sumsang",4000000,100,1);

INSERT INTO `items`(`name`, `description`, `price`, `stock`, `categories_id`) VALUES ("Uniklooh","baju keren dari brand ternama",500000,50,2);

INSERT INTO `items`(`name`, `description`, `price`, `stock`, `categories_id`) VALUES ("IMHO Watch","jam tangan anak yang jujur banget",2000000,10,1);

4. Select

4a. SELECT `id`, `name`, `email` FROM `users`;

4b. - SELECT * FROM `items` WHERE price > 1000000;
    - SELECT * FROM `items` WHERE name LIKE "Sang%"

4c. SELECT items.* , categories.name AS kategori FROM items INNER JOIN categories ON items.categories_id = categories.id;

5. Mengubah data dari database
UPDATE `items` SET `price`= 5000000 WHERE id=1;
