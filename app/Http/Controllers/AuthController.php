<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
   public function Register()
   {
    return view("register.register");
   }

   public function Sent(Request $request)
   {
        $firstName = $request['fname'];
        $lastName = $request['lname'];

        return view('register.home', ['firstName'=>$firstName,'lastName'=>$lastName]);
   }
}
