<?php

Require_once("Animal.php");
Require_once("ape.php");
Require_once("frog.php");

$sheep = new Animal("shaun");
echo "Nama: " . $sheep->name . "<br>";
echo "Jumlah legs: " . $sheep->legs . "<br>";
echo "Cold blooded: " . $sheep->cold_blooded . "<br><br>";

$sheep2 = new frog("Frog");
echo "Nama: " . $sheep2->name . "<br>";
echo "Jumlah legs: " . $sheep2->legs . "<br>";
echo "Cold blooded: " . $sheep->cold_blooded . "<br>";
echo "Jump: ". $sheep2->jump()."<br><br>";

$sheep3 = new ape("Ape");
echo "Nama : " . $sheep3->name . "<br>";
echo "Jumlah legs: " . $sheep3->legs . "<br>";
echo "Cold blooded: " . $sheep->cold_blooded . "<br>";
echo "Yell: ". $sheep3->yell()."<br>";

?>